### ChIPs- ChIP-seq analysis pipeline in snakemake

### Table of Contents
1. Introduction to ChIPs
2. Installing ChIPs
3. Using ChIPs

### 1. Introduction to ChIPs
The ChIPs pipeline is designed to perform robust quality control and reproducible processing of the chromatin profile sequencing data derived from ChIP-seq, DNase-seq, and ATAC-seq. 

CHIPS analysis is implemented across ten main procedures: 
- Read alignment
- Quality control
- Sample contamination 
- Copy number variation calling
- Peak calling
- Fraction of reads in peaks (FRIP) and PRC bottleneck (PBC) score
- Annotation with the cis-regulatory element annotation system (CEAS), including intersection with union DNase I hypersensitive sites (DHS)
- Gene target prediction 
- Motif enrichment
- Evolutionary conservation

- relation to ChiLin
- relation to viper

The inputs to the pipeline are FASTQ/BAM format DNA sequence read files, generated using sequencing protocols described in the Assays Section. 

The pipeline itself is encoded in the workflow language [snakemake](https://snakemake.readthedocs.io/en/stable/index.html) and executed in a conda environment using the cluster engine.


### 2. Installing ChIPs
You will only need to install ChIPs once, either for your own use, or if you are a system administrator, for the entire system (see **Appendix C**).   

# Required software
We assume that the following tools are already installed on your system and that you have some basic familiarity in using them:
`git`
`wget`
# Installing miniconda and/or mamba
It is recommended to create a dedicated software environment for ChIPs to avoid incompatibilities in required dependency packages. ChIPs uses the [Conda](https://conda.io/docs/intro.html) command line tool and python package combined with the [miniconda installer] (https://docs.conda.io/en/latest/miniconda.html) to manage environments and their software packages. Conda environments, explained [here](https://conda.io/docs/using/envs.html), are self-contained package spaces, similar to [Python Virtual Environments](http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/) or [Docker Containers](https://www.docker.com/what-container).

To install miniconda:

1. download the Miniconda installer:

    ```bash
    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
    ```

2. run the installer:

    ```bash
    bash Miniconda3-latest-Linux-x86_64.sh
    ```

3. update channels of conda:

    ```bash
    conda config --add channels conda-forge
    conda config --add channels bioconda
    ```

If installation of the ChIPs conda environment is too slow using conda, try using the conda reimplementation [mamba](https://github.com/mamba-org/mamba) instead. 

To install mamba:

    ```bash
    conda install mamba -c conda-forge
    ```

# Installing the ChIPs conda environment

After the environment installation tools are prepared, use them to install the ChIPs conda environment:
1. **clone the chips source code**:
    `git clone git@bitbucket.org:cfce/chips`
    ** NOTE: this command will create a directory called 'chips'.  After the next five steps, this directory can be safely deleted as we will explain how to *Setup a Chips Project* below. **

2. **create the chips environment**:
    After cloning the git repository, create the chips environment by doing this:
        ```bash
        cd cidc_chips
        conda env create -f environment.yml -n chips
        ```
    OR
        ```bash
        cd cidc_chips
        mamba env create -f environment.yml -n chips
        ```
        
3. **run setupChips.py**
    If you are in the 'chips' directory:
    
    ```bash
    conda activate chips
    python static/scripts/setupChips.py
    ```    
4. **configure [homer](http://homer.ucsd.edu/homer/motif/index.html) for motif analysis**:
    NOTE: ChIPs also has the capability of using the [MDSeqPos](https://github.com/XinDong9511/mdseqpos) motif finder for a similar analysis. If you are interested in using MDSeqPos for motif analysis, please see **Appendix D**.
    
    For human samples: 
    ```
    perl /path-to-homer/configureHomer.pl -install hg38
    perl /path-to-homer/configureHomer.pl -install hg19
    ```

    For mouse samples: 
    ```
    perl /path-to-homer/configureHomer.pl -install mm9
    ```


### 3. Using ChIPs
# Anatomy of a ChIPs project
All work in ChIPs is done in a **PROJECT** directory, which is simply a directory to contain a single ChIPs analysis run.  **PROJECT** directories can be named anything (and they usually start with a simple mkdir command, e.g. mkdir chips_for_paper),  but what is CRITICAL about a **PROJECT** directory is that you fill them with the following core components:
(We first lay out the directory structure and explain each element below.)
> PROJECT/  
> ├── cidc_chips/  
> ├── data/  - *optional*  
> ├── config.yaml  
> ├── metasheet.csv  
> ├── ref.yaml -  ***only if you are using chips OTHER THAN hg19 and mm9***   
> └── ref_files/

The 'cidc_chips' directory contains all of the chips source code.  We'll explain how to download that directory below.  The 'data' directory is an optional directory that contains all of your raw data. It is optional because those paths __may__ be fully established in the config.yaml, __however__ it is best practice to gather your raw data within 'data' using [symbolic links](https://www.cyberciti.biz/faq/creating-soft-link-or-symbolic-link/).

The *config.yaml* and *metasheet.csv* are configurations for your VIPER run (also explained below).

The ref.yaml file is explained in **Appendix E**.

After a successful **Chips** run, another 'analysis' folder is generated which contains all of the resulting output files.

The reference files (in ref_files) may be copied into the **PROJECT** directory from: ##make link to iris##
https://crazyhottommy.github.io/computation_wiki/setup/hpc-info/

# Setting up a ChIPs project
1. **creating the project directory**
    As explained above, the **PROJECT** directory is simply a directory to contain an entire Chips run.  **It can be named anything, but for this section, we'll simply call it 'PROJECT'**  
    
    ```bash
    mkdir PROJECT
    cd PROJECT
    ```
    
2. **link data files**
    As explained above, creating a data directory is a place to gather all of your **raw data files (.fastq, .fastq.gz, .bam)**.  It is optional, but **highly recommended**.
    
    ```bash
    mkdir data
    ```    
    
    And in 'data', copy over or make symbolic links to your raw data files.

3. **clone chips**
    In your PROJECT directory:  
    
    ```bash
    git clone git@bitbucket.org:cfce/chips
    ```   

4. **creating config.yaml and metasheet.csv**
    a. **copy chips/config.yaml and chips/metasheet.csv into the PROJECT dir:**
    In the PROJECT directory:  

    ```bash
    cp cidc_chips/config.yaml .
    cp cidc_chips/metasheet.csv .
    ``` 

    b. **setup config.yaml**:
    The config.yaml is where you define ChIPs run parameters and the ChIP-seq samples for analysis.
        
    - **Set the assembly**: typically hg19/hg38 or mm9/mm10 (default: hg38) 
    - **Choose the motif software**: either homer or MDSeqPos (default: homer)
    - **Contamination Panel**:
        The contamination panel is a panel that Chips will check for "cross-species" contamination.  Out of the box, the config.yaml has hg38 and mm9 as assemblies to check.  **IF you would like to add other species/assemblies, simply add as many BWA indices as you would like**
    - **Samples**:
        __The most important part of the config file is to define the samples for Chips analysis.__  
        Each sample is given an arbitrary name, e.g. MCF7_ER, MCF7_input, etc.  **Sample names, however, can not start with a number, and cannot contain '.', '-' (dash--use underscores instead)** (POSSIBLY others).  For each sample, define the path to the raw data file (.fastq, .fastq.gz, .bam).  For paired-end samples, simply add another line to define the path to the second pair.
    
    c. **setup metasheet.csv**:
    The metasheet.csv is where you group the **samples** (defined in config.yaml) into Treatment, Control (and if applicable, replicates).  For Chips, each of these groupings is called a **run**.  
    
    Open metasheet.csv in Excel or in a text-editor. You will notice the first (uncommented) line is:  
    `RunName,Treat1,Cont1,Treat2,Cont2`  
    
    **RunName**- arbitrary name for the run, e.g. *MCF7_ER_run*  
    **Treat1**- The sample name that corresponds to treatment sample.  **It must exactly match the sample name used in config.yaml**  
    **Cont1**- (optional) The input-control sample that should be paired with Treat1.  
    **Treat2**- (optional) if you have replicates, define the treatment of the replicate here.  
    **Cont2**- (optional) the input-control, if available, for Treat2  
    
4. setting up refs
    - change config.yaml ref: "cidc_chips/ref.yaml"
    - linking to static refs
    - copying ref.yaml

# Running ChIPs
It is recommended to submit a batch script to run ChIPs as opposed to running the job on an interactive node of the computing cluster. This is because running ChIPs tends to be memory intensive and such jobs on interactive nodes may be killed. An example batch script for the kraken high-performance computing system at Dana-Farber Cancer Institute is included for reference.

Acitivate the environment

```bash
conda activate chips
```

Perform a dry run

```bash
snakemake -np  -s cidc_chips/chips.snakefile --rerun-incomplete 
```

Perform a full run 

```bash
snakemake -s cidc_chips/chips.snakefile --rerun-incomplete -j 4
```

Example sbatch script for running ChIPs:

```bash
#!/bin/bash
#SBATCH --job-name=tcga_chips_sbatch
#SBATCH --mem=32G       # total memory need
#SBATCH --time=96:00:00
#SBATCH -c 8 #number of cores
#SBATCH -o log_%j.txt

conda init bash
conda activate chips
cd tcga_chips

snakemake -k -s cidc_chips/chips.snakefile -j 8 --rerun-incomplete
```


### Appendix A: System requirements
### Appendix B: Recommended requirements
### Appendix C: Installing ChIPs system-wide 
###### for system administrator, those who wish to share their ChIPs installation
### Appendix D: Installing the MDSeqPos motif finder for chips
### Appendix E: Generating static reference files for Chips

### Downloading the Chips static reference files
Chips comes pre-packaged with static reference files (e.g. bwa index, refSeq tables, etc.) for hg38 and mm9.
However, if you would like to use your own reference files **OR** if you are interested in sup, then please see **Appendix E**.
The ref.yaml file is explained in **Appendix E**.

```bash
conda activate chips
cd mdseqpos/lib
cp settings.py.example settings.py
```
Modify `settings.py` like below:
```python
#This should be absolute directory where you ref_files folder is.
ASSEMBLY_DIR = '***/***/ref_files' 
BUILD_DICT = { "hg19": "hg19/",
               "hg38": "hg38/",
               "mm9":"mm9/",
               "mm10": "mm10/"
               }
```
Then do:
```bash
cd ..
./version_updater.py
python setup.py install
```
At last, type `MDSeqPos.py` to ensure MDSeqPos is installed and check the usage.

### Appendix E: Generating static reference files for Chips
- all of the required files  
- using your own files
- supporting something new
- adding to ref.yaml
